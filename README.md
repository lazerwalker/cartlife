# Cart Life

This is a mirror of the source code of [Richard Hofmeier](http://www.richardhofmeier.com)'s magnificent game [Cart Life](http://www.richardhofmeier.com/cartlife). The original source code was downloaded off of his [Dropbox](https://www.dropbox.com/s/v2uu2cdonh3swxz/cartlife_opensores.zip).

For instructions on setup, check out `readme.html`, the original readme provided by Richard.
